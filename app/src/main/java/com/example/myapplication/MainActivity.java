package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView vue = (ListView) findViewById(R.id.listview);
        final AnimalList animalList = new AnimalList();


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);

        for(int i=0 ; i<animalList.getNameArray().length ;i++)
        {
            adapter.add(animalList.getNameArray()[i]);
        }

        vue.setAdapter(adapter);

        vue.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                Intent AnimalActivity = new Intent(MainActivity.this, AnimalActivity.class);
                AnimalActivity.putExtra("AnimalList",animalList);
                AnimalActivity.putExtra("position",position);
                startActivity(AnimalActivity);
            }
        });
    }


}
