package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        Intent intent = getIntent();
        final int position = intent.getIntExtra("position",0);
        final AnimalList animalList = (AnimalList) intent.getSerializableExtra("AnimalList");
        final TextView name = (TextView) findViewById(R.id.name);
        TextView esperance = (TextView) findViewById(R.id.Esperance);
        TextView periode = (TextView) findViewById(R.id.Periode);
        TextView poidsN = (TextView) findViewById(R.id.PoidsN);
        TextView poidsA = (TextView) findViewById(R.id.PoidsA);
        ImageView image = (ImageView) findViewById(R.id.imageView);
        Button button = (Button) findViewById(R.id.save);
        final EditText editText = (EditText) findViewById(R.id.Statut);

        String img = animalList.getAnimal(animalList.getNameArray()[position]).getImgFile();

        name.setText(animalList.getNameArray()[position]);
        esperance.setText(animalList.getAnimal(animalList.getNameArray()[position]).getStrHightestLifespan());
        periode.setText(animalList.getAnimal(animalList.getNameArray()[position]).getStrGestationPeriod());
        poidsN.setText(animalList.getAnimal(animalList.getNameArray()[position]).getStrBirthWeight());
        poidsA.setText(animalList.getAnimal(animalList.getNameArray()[position]).getStrAdultWeight());
        image.setImageResource(getResources().getIdentifier(img, "drawable", getPackageName()));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText().toString() != null)
                {
                    String tmp = editText.getText().toString();
                    animalList.getAnimal(animalList.getNameArray()[position]).setConservationStatus(tmp);
                }
            }
        });

    }

}
